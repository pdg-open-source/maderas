function makeMap(el) {
  const markers = el.querySelectorAll(".marker");
  const args = {
    zoom: 16,
    center: new google.maps.LatLng(0, 0),
    mapTypeId: google.maps.MapTypeId.ROADMAP
  };
  const map = new google.maps.Map(el, args);

  map.markers = [];

  markers.forEach(marker => {
    addMarker(marker, map);
  });

  // center map
  centerMap(map);

  // return
  return map;
}

function addMarker(marker, map) {
  const latlng = new google.maps.LatLng(marker.dataset.lat, marker.dataset.lng);
  const gMarker = new google.maps.Marker({
    position: latlng,
    map: map
  });
  map.markers.push(gMarker);
}

function centerMap(map) {
  const bounds = new google.maps.LatLngBounds();
  map.markers.forEach(marker => {
    const pos = new google.maps.LatLng(
      marker.position.lat(),
      marker.position.lng()
    );

    bounds.extend(pos);
  });

  if (map.markers.length == 1) {
    // set center of map
    map.setCenter(bounds.getCenter());
    map.setZoom(16);
  } else {
    // fit to bounds
    map.fitBounds(bounds);
  }
}

export { makeMap, addMarker, centerMap };
